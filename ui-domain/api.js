/**
 * Created by Andste on 2018/7/2.
 * base    : 基础业务API
 * buyer   : 买家API
 * seller  : 商家中心API
 * admin   ：后台管理API
 */

module.exports = {
  // 开发环境
  dev: {
    admin: window.__ENV__.API_ADMIN || 'http://localhost:8080'
  },
  // 测试环境
  test: {
    admin: window.__ENV__.API_ADMIN || 'http://erp-api.javastore.cn'
  },
  // 生产环境
  pro: {
    admin: window.__ENV__.API_ADMIN || 'http://erp-api.javastore.cn'
  }
}
