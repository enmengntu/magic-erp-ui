/**
 * Created by Andste on 2018/7/2.
 * admin    : 后台管理
 */

module.exports = {
  // 开发环境
  dev: {
    admin    : 'http://127.0.0.1:3003'
  },
  // 生产环境
  test: {
    admin    : window.__ENV__.DOMAIN_ADMIN     || 'http://erp.javastore.cn'
  },
  // 生产环境
  pro: {
    admin    : window.__ENV__.DOMAIN_ADMIN     || 'http://erp.javastore.cn'
  }
}
