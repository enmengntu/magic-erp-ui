#! /bin/bash

WEB_PATH=$PWD
cd $WEB_PATH


# 部署nodejs、nginx、yarn
if [[ "$1" == "base" ]];then
  # 移除yum lock
  rm -f /var/run/yum.pid

  # 关闭防火墙
  systemctl stop firewalld
  systemctl disable firewalld

  # 安装nodejs
  cd /usr/local/
  wget https://npm.taobao.org/mirrors/node/v14.17.6/node-v14.17.6-linux-x64.tar.gz
  tar -zxvf node-v14.17.6-linux-x64.tar.gz
  rm -rf node-v14.17.6-linux-x64.tar.gz
  mv node-v14.17.6-linux-x64 node
  echo 'export NODE_HOME=/usr/local/node
export PATH=$NODE_HOME/bin:$PATH'>>/etc/profile
  source /etc/profile

  # node npm 链接
  sudo ln -s /usr/local/node/bin/node /usr/bin/node
  sudo ln -s /usr/local/node/bin/node /usr/lib/node
  sudo ln -s /usr/local/node/bin/npm /usr/bin/npm

  # 安装yarn
  npm install yarn -g --registry=https://registry.npmmirror.com

  # yarn 链接
  sudo ln -s /usr/local/node/bin/yarn /usr/bin/yarn
  sudo ln -s /usr/local/node/bin/yarn /usr/lib/yarn

  # nginx安装
  yum install -y gcc gcc-c++
  yum install -y pcre pcre-devel
  yum install -y zlib zlib-devel
  yum install -y openssl openssl-devel

  cd /usr/local
  wget https://nginx.org/download/nginx-1.14.0.tar.gz
  tar -zxvf nginx-1.14.0.tar.gz
  cd nginx-1.14.0
  ./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module
  make && make install
  cd ..
  rm -rf nginx-1.14.0.tar.gz
  rm -rf nginx-1.14.0
fi

if [[ ! -n "$1" || "$1" == "manager-admin" || "$1" == "uni" || "$1" == "test" ]];then
  echo "开始部署后台管理(manager-admin)..."
  cd $WEB_PATH/manager-admin
  rm -rf dist
  rm -rf node_modules
  echo "开始安装项目依赖..."
  yarn --registry=https://registry.npmmirror.com
  echo "开始打包..."
  if [[ "$1" == "test" || "$2" == "test" ]];then
    yarn run build:test
  else
    yarn run build:prod
  fi
  echo -e "\033[32m后台管理部署完成...\033[0m"
fi

