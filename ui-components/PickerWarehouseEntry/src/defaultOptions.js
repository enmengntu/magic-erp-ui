import request from '@/utils/request'

export const props = {
  // 显示选择器
  show: {
    type: Boolean,
    default: false
  },
  // 已选导购的ID集合
  selectedIds: {
    type: Array,
    required: false,
    default: () => ([])
  },
  // 获取导购列表API
  goodsApi: {
    type: String,
    required: true
  },
  // 扩展的导购API参数
  purchasePlanApiParams: {
    type: Object,
    default: () => ({})
  },
  // 请求方法
  request: {
    type: Function,
    required: false,
    default: request
  },
  // 最大可选个数
  limit: {
    type: Number,
    default: -1
  },
  // 是否是管理端
  isAdmin: {
    type: Boolean,
    default: true
  }
}

export const data = {
}