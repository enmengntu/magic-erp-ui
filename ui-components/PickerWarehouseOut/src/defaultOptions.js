import request from '@/utils/request'

export const props = {
  // 显示选择器
  show: {
    type: Boolean,
    default: false
  },
  // 已选订单的ID集合
  selectedIds: {
    type: Array,
    required: false,
    default: () => ([])
  },
  // 获取订单列表API
  orderApi: {
    type: String,
    required: true
  },
  // 订单API参数
  orderApiParams: {
    type: Object,
    default: () => ({})
  },
  // 请求方法
  request: {
    type: Function,
    required: false,
    default: request
  },
  // 最大可选个数
  limit: {
    type: Number,
    default: -1
  },
  // 扩展列
  columns: {
    type: Array,
    default: () => (generalColumns)
  },
  // 是否是管理端
  isAdmin: {
    type: Boolean,
    default: true
  }
}

export const data = {
}

// 一般列
export const generalColumns = [
  { label: '订单编号', prop: 'sn' },
  { label: '下单时间', prop: 'create_time' },
  { label: '下单会员', prop: 'member_name' }
]
