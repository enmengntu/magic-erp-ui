/**
 * 公共API
 */

import { api } from '~/ui-domain'
import request from '@/utils/request'
import Storage from '@/utils/storage'

/**
 * 获取图片验证码URL
 * @param scene
 * @param uuid
 * @returns {string}
 */
export function getValidateCodeUrl(scene, uuid) {
  return `${api.admin}/captchas/${uuid}/${scene}?rmd=${new Date().getTime()}`
}

/**
 * 获取登陆验证方式
 */
export function getValidator() {
  return request({
    url: `${api.admin}/validator`,
    method: 'GET',
    needToken: false
  })
}

/**
 * 刷新token
 */
export function refreshToken() {
  return request({
    url: 'admin/systems/admin-users/token',
    method: 'post',
    needToken: false,
    data: {
      refresh_token: Storage.getItem('admin_refresh_token')
    }
  })
}

/**
 * 加盟商刷新Token
 * @returns {*}
 */
export function refreshFranchiseToken() {
  return request({
    url: `/admin/shops/franchise/token`,
    method: 'post',
    needToken: false,
    data: {
      refresh_token: Storage.getItem('admin_refresh_token')
    }
  })
}

/**
 * 获取首页数据
 */
export function getIndexData() {
  return request({
    url: 'admin/index/page',
    method: 'get',
    loading: false
  })
}
/**
 * 获取加盟商首页数据
 */
export function getFranchiseIndexData() {
  return request({
    url: '/admin/shops/franchise/index/page',
    method: 'get',
    loading: false
  })
}

/**
 * 获取站点信息
 * @returns {*}
 */
export function getSiteInfo() {
  return request({
    url: `${api.admin}/site-show`,
    method: 'get',
    needToken: false,
    loading: false
  })
}
