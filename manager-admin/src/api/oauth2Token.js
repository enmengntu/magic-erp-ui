/**
 * OAuth2 Token 相关API
 */

import request from '@/utils/request'

/**
 * 分页列表
 * @param params
 */
export function getList(params) {
  return request({
    url: '/admin/systems/oauth2-token',
    method: 'get',
    params
  })
}

/**
 * 删除
 * @param access_tokens
 */
export function del(access_tokens) {
  return request({
    url: `/admin/systems/oauth2-token/${access_tokens}`,
    method: 'delete'
  })
}
