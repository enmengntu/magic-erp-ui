/**
 * 商品换货相关API
 */

import request from '@/utils/request'

/**
 * 查询产品借出数量信息
 * @param params
 */
export function getlistProductLendNum(params) {
  return request({
    url: 'admin/lend/form/list-product-lend-num',
    method: 'post',
    loaidng: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 归还商品换货
 * @param id
 * @param parmas
 */
export function returnGoodsInventory(id, parmas) {
  return request({
    url: `admin/stock/inventory/return/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 批量确认商品换货单
 * @param id
 */
export function inventoryFormConfirm(ids) {
  return request({
    url: `admin/stock/inventory/confirm/${ids}`,
    method: 'post'
  })
}

/**
 * 修改商品换货
 * @param id
 * @param parmas
 */
export function editgoodsInventoryAdd(id, parmas) {
  return request({
    url: `admin/stock/inventory/${id}`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 添加商品换货
 * @param params
 */
export function addgoodsInventoryAdd(params) {
  return request({
    url: 'admin/stock/inventory',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 提交审核商品换货单
 * @param ids
 */
export function submitAuditGoodsInventory(ids) {
  return request({
    url: `admin/stock/inventory/submit/${ids}`,
    method: 'post'
  })
}

/**
 * 撤回商品换货
 * @param ids
 */
export function cancelGoodsInventory(ids) {
  return request({
    url: `admin/stock/inventory/cancel/${ids}`,
    method: 'post'
  })
}

/**
 * 审核商品换货
 * @param params
 */
export function auditGoodsInventory(ids, parmas) {
  return request({
    url: `admin/stock/inventory/audit/${ids}`,
    method: 'post',
    data: parmas
  })
}

/**
 * 获取商品换货详情
 * @param params
 */
export function getGoodsInventoryInfo(id) {
  return request({
    url: `admin/stock/inventory/${id}`,
    method: 'get',
    loaidng: false
  })
}

/**
 * 获取商品换货列表
 * @param params
 */
export function getGoodsInventoryList(params) {
  return request({
    url: 'admin/stock/inventory',
    method: 'get',
    loaidng: false,
    params
  })
}

/**
 * 更新商品换货
 * @param id
 * @param parmas
 */
export function editGoodsInventory(id, parmas) {
  return request({
    url: `admin/stock/inventory/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: parmas
  })
}

/**
 * 删除商品换货
 * @param id
 */
export function deleteGoodsInventory(ids) {
  return request({
    url: `admin/stock/inventory/${ids}`,
    method: 'delete'
  })
}
