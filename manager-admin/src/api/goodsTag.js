import request from '@/utils/request'

/**
 * 获取标签列表
 * @param params
 * @returns {Promise<any>}
 */
export function getTagsList(params) {
  return request({
    url: 'admin/erp/goodsLabel',
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 添加标签
 * @param params
 * @returns {Promise<any>}
 */
export function add(params) {
  return request({
    url: 'admin/erp/goodsLabel',
    method: 'post',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 编辑标签
 * @param params
 * @returns {Promise<any>}
 */
export function edit(id, params) {
  return request({
    url: `admin/erp/goodsLabel/${id}`,
    method: 'put',
    loading: false,
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}

/**
 * 删除标签
 * @param params
 * @returns {Promise<any>}
 */
export function del(id) {
  return request({
    url: `admin/erp/goodsLabel/${id}`,
    method: 'delete',
    loading: false
  })
}

/**
 * 获取标签商品列表
 * @param params
 * @returns {Promise<any>}
 */
export function getTagGoodsList(params) {
  return request({
    url: `/admin/erp/goodsLabelRelation`,
    method: 'get',
    loading: false,
    params
  })
}

/**
 * 保存标签商品列表
 * @param params
 * @returns {Promise<any>}
 */
export function saveTagGoodsList(params) {
  return request({
    url: `/admin/erp/goodsLabelRelation/relationGoods`,
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: params
  })
}
