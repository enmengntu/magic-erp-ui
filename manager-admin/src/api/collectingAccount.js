/**
 * 支付方式管理相关API
 */

import request from '@/utils/request'

/**
 * 分页列表
 * @param params
 */
export function getList(params) {
  return request({
    url: '/admin/erp/collectingAccount',
    method: 'get',
    params
  })
}

/**
 * 添加
 * @param data
 */
export function add(data) {
  return request({
    url: '/admin/erp/collectingAccount',
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    data: data
  })
}

/**
 * 查询详情
 * @param id
 */
export function getDetail(id) {
  return request({
    url: `admin/erp/collectingAccount/${id}`,
    method: 'get'
  })
}

/**
 * 修改
 * @param id
 * @param data
 */
export function edit(id, data) {
  return request({
    url: `admin/erp/collectingAccount/${id}`,
    method: 'put',
    headers: { 'Content-Type': 'application/json' },
    data: data
  })
}

/**
 * 删除
 * @param id
 */
export function del(id) {
  return request({
    url: `admin/erp/collectingAccount/${id}`,
    method: 'delete'
  })
}

/**
 * 修改是否启用状态
 * @param id
 * @param enable_flag
 */
export function updateEnableFlag(id, enable_flag) {
  return request({
    url: `admin/erp/collectingAccount/${id}/enableFlag`,
    method: 'put',
    params: { enable_flag }
  })
}

/**
 * 设为默认
 * @param id
 */
export function updateDefault(id) {
  return request({
    url: `admin/erp/collectingAccount/${id}/default`,
    method: 'put'
  })
}
